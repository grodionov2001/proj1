from django.shortcuts import render
from django.views.generic import View, DetailView, ListView, TemplateView


class IndexView(TemplateView):
    template_name = "page.html"
